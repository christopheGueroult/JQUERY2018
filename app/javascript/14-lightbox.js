$(function(){

	let nbImg;
	let index;
	let src;

	nbImg = $('[data-galery] img').length;

	let changeImg = function() {
		// get attr src img suivante
		src = $('[data-galery] img').eq(index).attr('src');
		// set attr src img lightbox
		$('[data-lightbox] img').attr('src', src);
		changePuce();
	}

	// générer autant de puces qu'il y a d'img dans la galery
	let generatePuces = function() {
		for (i=0 ; i<nbImg ; i++) {
			$('[data-lightbox] ul')
			.append('<li><i class="icon-circle-o"></i></li>');
		}
	}

	// change puces pour colorier puce img sélectionnée
	let changePuce = function() {
		$('[data-lightbox] i')
		.removeClass('icon-circle')
		.addClass('icon-circle-o')
		.eq(index)
		.addClass('icon-circle')
		.removeClass('icon-circle-o');
	}

	generatePuces();
	
	// quand on clique sur une img on ouvre la lightbox
	$('[data-galery] img').click(function(){
		// get index img cliquée
		index = $('[data-galery] img').index( $(this) );
		// get attr src img cliquée
		src = $(this).attr('src');
		// set attr src img lightbox
		$('[data-lightbox] img').attr('src', src);
		// ouvre la lightbox
		$('[data-lightbox]').fadeIn().css({'display':'flex'});
		// colorise puce qui correspond à l'img cliquée
		changePuce();
	});
	
	// fermer la lightbox quand on clique sur icon-close
	$('[data-lightbox] .icon-close').click(function(){
		$('[data-lightbox]').fadeOut();
	});

	// quand on clique sur icon-angle-right
	$('[data-lightbox] .icon-angle-right').click(function(){
		index = (index + 1)%nbImg;
		changeImg();
	});

	// quand on clique sur icon-angle-left
	$('[data-lightbox] .icon-angle-left').click(function(){
		index = (index - 1 + nbImg)%nbImg; // permet de rester en positif
		// index = (index - 1)%nbImg; // passe en negatif. fonctionne car tableau parcouru à l'envers
		changeImg();
	});

	// quand on clique sur une puce 
		// on recupere l'index de la puce cliquee et on l'enregistre dans variable index
		// changeImg()
	$('[data-lightbox] i').click(function(){
		index = $('[data-lightbox] i').index( $(this) );
		changeImg();
	});
	


});   