$(function(){
	

	$('button:first-of-type').click(function(){
		// click pour modifier text du permier p
		$('p:first-of-type').text('whaouh top fort jQuery !');
	});

	$('button:nth-of-type(2)').dblclick(function(){
		// double click pour modifier text du premier p
		$('p:first-of-type').text('wouuuuh, un double click !');
	});

	$('button:nth-of-type(3)').mouseenter(function(){
		// mouse enter pour modifier text du premier p
		$('p:first-of-type').text('je kiffe les mouse enter !');
	});

	$('button:nth-of-type(4)').mouseleave(function(){
		// mouse leave pour modifier text du premier p
		$('p:first-of-type').text('mouse leave works !');
	});

	$('button:nth-of-type(5)').hover(function(){
		// hover pour modifier text du premier p quand on entre  
		// dessus puis quand on le quitte
		$('p:first-of-type').text('mouse enter works !');
	}, function(){
		$('p:first-of-type').text('mouse leave works !');
	});

	
	$('input').focus(function(){
		$(this).val('ce champ prend le focus');
	});

	$('input').blur(function(){
		$(this).val('ce champ perd le focus');
	});

	$(window).keyup(function(){
		$('p:first-of-type').text('touche relevée !');
	});

}); 