$(function(){
	

	$('button:first-of-type').click(function(){
		// position elem en left 500 avec une animation
		// en fonction d'une durée
		$('.blue').animate({'left': 500}, 1000);
		$('.red').animate({'left': 500}, 800);
		$('.green').animate({'left': 500}, 1200);
	});

	// ecrivez le code pour le bouton reset qui remet 
	// tous les div en position left 0

	$('button:nth-of-type(5)').click(function(){
		// position elem en left 500 avec une animation
		// en fonction d'une durée
		$('div').animate({
			'left': 0, 
			'top': 0, 
			'width': '100px',
			'height': '100px' 
		}, 200);
	});

	$('button:nth-of-type(2)').click(function(){
		// animate on mutiple css properties
		$('.blue').animate({
			'left': 500,
			'top': 500,
			'width': '200px',
			'height': '200px'
		}, 1000);
	});

	$('button:nth-of-type(3)').click(function(){
		// animate avec chainage
		$('.blue')
		.animate({
			'left': 500,
			'top': 500,
			'width': '200px',
			'height': '200px'
		}, 1000)
		.animate({
			'left': 0, 
			'top': 0, 
			'width': '100px',
			'height': '100px' 
		}, 200);
	});

	// exercice : prendre un block au hasard, chainer plusieurs
	// animations et le faire revenir à sa place initiale

	$('button:nth-of-type(4)').click(function(){
		let e = $('.red');
		e.animate({
			'left': 150, 
			'top': 50, 
		}, 500)
		.animate({
			'left': 350, 
			'top': 500, 
		}, 200)
		.animate({
			'left': 550, 
			'top': 0, 
		}, 1000)
		.animate({
			'left': 0, 
			'top': 0, 
		}, 100)
	});



	

}); 