$(function(){

	$('[data-form] [name]').blur(function(){
		// si la valeur d'un champ est vide
		if ($(this).val() == '') {
			// on ajoute la class 'invalid' sur le label qui suit ce champs
			// et on retire class
			$(this).next().addClass('invalid').removeClass('valid');
		} else {
			// on ajoute la class 'valid' sur le label qui suit ce champs
			// et retirer class 'invalid'
			$(this).next().addClass('valid').removeClass('invalid');
		}
	});

	$('[data-form]').submit(function(){
		let valid = true;
		$('[data-form] [required]').each(function(){
			if ($(this).val() == '') {
				valid = false;
				$(this).next().addClass('invalid').removeClass('valid');
			}
		});
		if (valid) {
			// $(this).reset();
			$.ajax({
				type: "POST",
				url: "envoimail.php",
				data:  $(this).serialize(),
				success: function (response) {
					if (response == 'formulaire invalid') {
						// traite reponse
						// add class invalid sur champ mal rempli
						// affiche text echec
						$('[data-form] .popup').text('champ invalid').fadeIn();
					} else {
						// traite la reponse
						$('[data-form] [name]').each(function(){
							$(this).val() = '';
						});
						$('[data-form] .popup').text('envoi réussi').fadeIn();
					}
				}
			});
		}
		return false;
	});

});   

