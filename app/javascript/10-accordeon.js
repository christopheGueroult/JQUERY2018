$(function(){
	

	// 1. quand on clique sur un h1, on referme tous les p
	// 2. on ouvrre la réponse juste après la question cliquée
	// 3. mettre la class icon-angle-down sur tous les span
	// 4. mettre la class icon-angle-up sur le span qui correspond à la question cliquée
	// 5. supprimer class active sur tous les h1
	// 6. ajouter la class active sur le h1 cliqué
	$('[accordeon] h1').click(function(){
		$('[accordeon] p').slideUp(100);
		$(this).next().slideDown(500);
		$('[accordeon] span').attr('class', 'icon-angle-down');
		$(this).children('span').attr('class', 'icon-angle-up');
		$('[accordeon] h1').removeClass('active');
		$(this).addClass('active');
	});
	

}); 