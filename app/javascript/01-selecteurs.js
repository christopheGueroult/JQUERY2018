// jQuery(document).ready(function(){});
// $(document).ready(function(){});
$(function(){
	// ici ts le jquery
	// $('selecteur css').methodeJquery(...args?);

	// $('selecteur css').event(function(...args?){
	// 	$('selecteur css').methodeJquery(...args?);
	// });

	$('button:first-of-type').click(function(){
		// masquer ts les p du dom
		$('p').hide(1000);
	});

	$('button:nth-of-type(2)').click(function(){
		// afficher ts les p du dom
		$('p').show(1000);
	});

	$('button:nth-of-type(3)').click(function(){
		// masquer 1er li de ul
		$('ul li:first-child').hide(1000);
	});

	$('button:nth-of-type(4)').click(function(){
		// masquer ts les elem du dom qui ont un attr href
		$('[href]').hide(1000);
	});

	$('button:nth-of-type(5)').click(function(){
		// afficher ts les elem du dom qui ont 
		// l'attr href et la valeur http://orsys.fr
		$('[href="http://orsys.fr"]').show(1000);
	});

	$('button:nth-of-type(6)').click(function(){
		// masquer ts les elem du dom qui ont 
		// l'attr href et une valeur qui contient http
		$('[href*="http"]').hide(1000);
	});



});