$(function(){
	

	$('button:first-of-type').click(function(){
		$('ul').before('<p>elem ajouté devant ul avec before()</p>');
	});
	
	$('button:nth-of-type(2)').click(function(){
		$('ul').after('<p>elem ajouté après ul avec after()</p>');
	});
	
	$('button:nth-of-type(3)').click(function(){
		$('ul').prepend('<li>item pas</li>');
	});
	
	$('button:nth-of-type(4)').click(function(){
		$('ul').append('<li>item beaucoup</li>');
	});
	
	let compteur = 0;
	$('button:nth-of-type(5)').click(function(){
		compteur++;
		if (compteur <= 7) {
			$('ul').append('<li>item beaucoup</li>');
		} else {
			alert('c\'est bon je te kiff t\'as pas compris ?');
		}
	});
	
	let compt = 1;
	$('button:nth-of-type(6)').click(function(){
		compt--;
		if (compt < 0) {
			alert('c\'est bon il t\'aime PAS il a dit !');
		} else {
			$('ul').prepend('<li>item vraiment pas</li>');
		}
	});

	let i = 3;
	$('button:nth-of-type(7)').click(function(){
		i++;
		if (i <= 10) {
			$('ul').append('<li>item ' + i + '</li>');
		} else {
			alert('limite atteinte');
		}
	});
	
	let j = 1;
	$('button:nth-of-type(8)').click(function(){
		j--;
		if (j < 0) {
			alert('limite atteinte');
		} else {
			$('ul').prepend('<li>item ' + j + '</li>');
		}
	});

	// récupérer dernier caractère du dernier li
	let chaine = $("li").last().text(); // => return item 3
	// caster dernier caractère en number
	let lastChar = parseInt(chaine[chaine.length - 1]); // => return 3 (int)
	// assigner cette valeur à la variable k
	let k = lastChar;
	$('button:nth-of-type(9)').click(function(){
		k++;
		if (k <= 10) {
			$('ul').append('<li>item ' + k + '</li>');
		} else {
			alert('limite atteinte');
		}
	});

	// récupérer dernier caractère du 1er li
	let chaine2 = $("li").first().text(); // => return item 1
	// caster dernier caractère en number
	let lastChar2 = parseInt(chaine2[chaine2.length - 1]); // => return 1 (int)
	// assigner cette valeur à la variable l
	let l = lastChar2;
	$('button:nth-of-type(10)').click(function(){
		l--;
		if (l < 0) {
			alert('limite atteinte');
		} else {
			$('ul').prepend('<li>item ' + l + '</li>');
		}
	});
	


}); 