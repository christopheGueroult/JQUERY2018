$(function(){
	
	// 1. au chargement de la page, definir un maxlength
	// 2. modifier attr maxlength du textarea
	// 3. modifier contenu text de balise small
	// 4. dès que user saisi un lettre on veut compter le nombre de lettre saisies
	// 5. on veut soustraire ce nombre à max
	let max = 40;
	let lChaine;
	let pastedData;
	$('[compteur] textarea').attr('maxlength', max);
	$('[compteur] small').text(max);

	$('[compteur] textarea').keyup(function(){
		lChaine = $(this).val().length;
		$('[compteur] small').text(max - lChaine);
	});

	// pour les copier coller
	
	$('[compteur] textarea').bind('paste', function(e){
		pastedData = e.originalEvent.clipboardData.getData('text');
		lChaine = pastedData.length + $(this).val().length;
		if (lChaine < max) {
			$('[compteur] small').text(max - lChaine); 
		} else {
			$('[compteur] small').text('0');  
		}
	});



}); 