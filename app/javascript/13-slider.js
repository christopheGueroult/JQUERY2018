$(function(){
	// au chargement de la page on autoriste le user la cliquer sur un
	// bouton pour defiler en left ou en right
	let acceptDefil = true;
	// recupérer largeur slider pour l'affecter aux li
	let l = $('[data-slider]').width();
	$('[data-slider] li').width(l);

	// quand on resiz la fenetre
		// recalculer la largeur du slider
		// ré-affecter cette largeur aux li
	$(window).resize(function(){
		l = $('[data-slider]').width();
		$('[data-slider] li').width(l);
	});

	// barre de progression
	let animation = function() {
		$('[data-slider] .progress').animate({'width':'100%'}, 2000, function(){
			$(this).css({'width':'0%'});
		});
	}

	// 1. créer une fonction pour : animer ul en left -?
	// 2. une fois animation terminée : 
		// 2.1 mettre 1er li après le dernier li
		// 2.2 repositionner le ul en left 0
	let defilRight = () => {
		$('[data-slider] ul').animate( {'left': -l}, 1000, function(){
			$('[data-slider] li:last').after( $('[data-slider] li:first') );
			$(this).css({'left': 0});
			acceptDefil = true;
			animation();
		} );
	}

	// 1. positionner ul en left -? sans animation en utilisant la methode css()
	// 2. dans le meme temps, mettre dernier li devant 1er li
	// 3. animer ul de left -? vers left 0 en une seconde

	let defilLeft = function() {
		$('[data-slider] ul').css({'left': -l});
		$('[data-slider] li:first').before( $('[data-slider] li:last') );
		$('[data-slider] ul').animate({'left': 0}, 1000, function(){
			acceptDefil = true;
			animation();
		});
	}

	// quand on clique sur btn left 
		// 1. stoper le defilement automatique
		// 2. on veut appeler une fois defilLeft
		// 3. on accept le defilement uniquement si anim terminee
	$('[data-slider] .icon-angle-left').click(function(){
		if (acceptDefil) {
			acceptDefil = false;
			clearInterval(interval);
			defilLeft();
		}
	});

	// idem quand on clique sur btn right
		// 1. stop defilement auto
		// 2. on appel une fois defilRight
		// 3. on accept defilement si anim terminee
	$('[data-slider] .icon-angle-right').click(function(){
		if (acceptDefil) {
			acceptDefil = false;
			clearInterval(interval);
			defilRight();
		}
	});
	
	let interval = setInterval(defilRight, 3000);

});  